import 'package:dio/dio.dart';
import 'package:nike_app/data/repo/auth_repository.dart';

final httpClient = Dio(
  BaseOptions(baseUrl: 'http://expertdevelopers.ir/api/v1/'),
)..interceptors.add(InterceptorsWrapper(
    onRequest: (options, handler) {
      final authinfo = Authrepository.authChangeNotifier.value;
      if (authinfo != null && authinfo.accessToken.isNotEmpty) {
        options.headers['Authorization'] = 'Bearer ${authinfo.accessToken}';
      }
      handler.next(options);
    },
  ));
