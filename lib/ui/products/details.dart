import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nike_app/common/utils.dart';
import 'package:nike_app/data/product.dart';
import 'package:nike_app/data/repo/cart_repository.dart';
import 'package:nike_app/theme.dart';
import 'package:nike_app/ui/products/bloc/product_bloc.dart';

import 'package:nike_app/ui/products/comment/comment_List.dart';
import 'package:nike_app/ui/widgets/image.dart';

class ProductDetailScreen extends StatefulWidget {
  final ProductEntity product;
  const ProductDetailScreen({Key? key, required this.product}) : super(key: key);

  @override
  State<ProductDetailScreen> createState() => _ProductDetailScreenState();
}

class _ProductDetailScreenState extends State<ProductDetailScreen> {
  StreamSubscription<ProductState>? stateSubscription;

  final GlobalKey<ScaffoldMessengerState> _scaffoldKey = GlobalKey();
  @override
  void dispose() {
    _scaffoldKey.currentState?.dispose();
    stateSubscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: BlocProvider<ProductBloc>(
        create: (context) {
          final bloc = ProductBloc(cartRepository);
          stateSubscription = bloc.stream.listen((state) {
            if (state is ProductAddToCartSuccess) {
              _scaffoldKey.currentState?.showSnackBar(const SnackBar(content: Text('به سبد خرید شما اضافه شد ')));
            } else if (state is ProductaddToCartError) {
              _scaffoldKey.currentState?.showSnackBar(SnackBar(content: Text(state.appExeption.message)));
            }
          });
          return bloc;
        },
        child: ScaffoldMessenger(
          key: _scaffoldKey,
          child: Scaffold(
            floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
            floatingActionButton: SizedBox(
              width: MediaQuery.of(context).size.width - 48,
              child: BlocBuilder<ProductBloc, ProductState>(
                builder: (context, state) => FloatingActionButton.extended(
                    onPressed: () {
                      BlocProvider.of<ProductBloc>(context).add(CartAddButtonClicked(widget.product.id));
                    },
                    label: state is ProductAddToCartButtonLoading
                        ? const CupertinoActivityIndicator(
                            color: Colors.white,
                          )
                        : Text('افزودن به سبد خرید ')),
              ),
            ),
            body: CustomScrollView(
              slivers: [
                SliverAppBar(
                  expandedHeight: MediaQuery.of(context).size.width * 0.8,
                  flexibleSpace: ImageLoadingService(imageURL: widget.product.imageurl),
                  foregroundColor: LightThemecolors.primarytextcolor,
                  actions: [IconButton(onPressed: () {}, icon: const Icon(CupertinoIcons.heart))],
                ),
                SliverToBoxAdapter(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                widget.product.title,
                                style: Theme.of(context).textTheme.headline6!.copyWith(fontSize: 18),
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  widget.product.previousprice.withPriceLabel,
                                  style: Theme.of(context).textTheme.caption!.apply(decoration: TextDecoration.lineThrough),
                                ),
                                Text(widget.product.price.withPriceLabel),
                              ],
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 24,
                        ),
                        const Text('این کتونی شدیدا برای دویدن و راه رفتن مناسب هست و تقریبا. هیچ فشار مخربی رو نمیذارد به پا و زانوان شما انتقال داده شود'),
                        const SizedBox(
                          height: 24,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'نظرات کاربران',
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                            TextButton(
                              onPressed: () {},
                              child: const Text('ثبت نظر'),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                CommentList(productid: widget.product.id),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
