import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:nike_app/common/exeption.dart';
import 'package:nike_app/data/repo/Product_repository.dart';
import 'package:nike_app/data/repo/cart_repository.dart';

part 'product_event.dart';
part 'product_state.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  final ICartRepository cartRepository;
  ProductBloc(this.cartRepository) : super(ProductInitial()) {
    on<ProductEvent>((event, emit) async {
      if (event is CartAddButtonClicked) {
        try {
          emit(ProductAddToCartButtonLoading());
          Future.delayed(Duration(seconds: 2));
          final resualt = await cartRepository.add(event.productid);
          emit(ProductAddToCartSuccess());
        } catch (e) {
          emit(ProductaddToCartError(AppExeption()));
        }
      }
    });
  }
}
