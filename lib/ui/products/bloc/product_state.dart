part of 'product_bloc.dart';

abstract class ProductState extends Equatable {
  const ProductState();

  @override
  List<Object> get props => [];
}

class ProductInitial extends ProductState {}

class ProductAddToCartButtonLoading extends ProductState {}

class ProductaddToCartError extends ProductState {
  final AppExeption appExeption;
  const ProductaddToCartError(this.appExeption);
  @override
  // TODO: implement props
  List<Object> get props => [appExeption];
}

class ProductAddToCartSuccess extends ProductState {}
