import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nike_app/common/exeption.dart';
import 'package:nike_app/data/comment.dart';
import 'package:nike_app/data/repo/comment_repository.dart';
import 'package:nike_app/ui/products/comment/bloc/commentlistbloc_bloc.dart';
import 'package:nike_app/ui/widgets/error.dart';

class CommentList extends StatelessWidget {
  final int productid;
  const CommentList({Key? key, required this.productid}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) {
        final bloc = CommentListBloc(
            productid: productid, repository: commentRepository);
        bloc.add(CommentListStarted());
        return bloc;
      },
      child: BlocBuilder<CommentListBloc, CommentListState>(
        builder: (context, state) {
          if (state is CommentListSucces) {
            return SliverList(
              delegate: SliverChildBuilderDelegate((context, index) {
                return CommentsItem(
                  data: state.comments[index],
                );
              }, childCount: state.comments.length),
            );
          } else if (state is CommentlistLoading) {
            return const SliverToBoxAdapter(
              child: Center(child: CircularProgressIndicator()),
            );
          } else if (state is CommentsError) {
            return SliverToBoxAdapter(
              child: AppErrorWidget(
                appExeption: state.exeption,
                ontap: () {
                  BlocProvider.of<CommentListBloc>(context)
                      .add(CommentListStarted());
                },
              ),
            );
          } else {
            throw Exception('state not Suported');
          }
        },
      ),
    );
  }
}

class CommentsItem extends StatelessWidget {
  final CommentEntity data;
  const CommentsItem({
    required this.data,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Theme.of(context).dividerColor, width: 1),
        borderRadius: BorderRadius.circular(4),
      ),
      padding: EdgeInsets.fromLTRB(8, 0, 8, 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(data.title),
                  const SizedBox(
                    height: 4,
                  ),
                  Text(
                    data.email,
                    style: Theme.of(context).textTheme.caption,
                  )
                ],
              ),
              Text(
                data.date,
                style: Theme.of(context).textTheme.caption,
              ),
            ],
          ),
          SizedBox(
            height: 16,
          ),
          Text(data.content),
        ],
      ),
    );
  }
}
