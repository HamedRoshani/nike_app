import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:nike_app/common/exeption.dart';
import 'package:nike_app/data/comment.dart';
import 'package:nike_app/data/repo/comment_repository.dart';

part 'commentlistbloc_event.dart';
part 'commentlistbloc_state.dart';

class CommentListBloc extends Bloc<CommentListEvent, CommentListState> {
  final ICommentrepository repository;
  final int productid;

  CommentListBloc({required this.repository, required this.productid})
      : super(CommentlistLoading()) {
    on<CommentListEvent>((event, emit) async {
      if (event is CommentListStarted) {
        emit(CommentlistLoading());
        try {
          final comments = await repository.getAll(productid: productid);
          emit(CommentListSucces(comments));
        } catch (e) {
          emit(CommentsError(AppExeption()));
        }
      }
    });
  }
}
