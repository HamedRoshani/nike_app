part of 'commentlistbloc_bloc.dart';

abstract class CommentListState extends Equatable {
  const CommentListState();

  @override
  List<Object> get props => [];
}

class CommentlistLoading extends CommentListState {}

class CommentListSucces extends CommentListState {
  final List<CommentEntity> comments;

  const CommentListSucces(this.comments);
  @override
  List<Object> get props => [comments];
}

class CommentsError extends CommentListState {
  final AppExeption exeption;

  const CommentsError(this.exeption);

  @override
  List<Object> get props => [exeption];
}
