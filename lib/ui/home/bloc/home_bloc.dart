import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:nike_app/common/exeption.dart';
import 'package:nike_app/data/banner.dart';
import 'package:nike_app/data/product.dart';
import 'package:nike_app/data/repo/Banner_repository.dart';
import 'package:nike_app/data/repo/Product_repository.dart';
part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final IBannerRepository bannerRepository;
  final IProductRepository productRepository;
  HomeBloc({required this.bannerRepository, required this.productRepository}) : super(HomeLoading()) {
    on<HomeEvent>((event, emit) async {
      if (event is HomeStarted || event is Homerefresh) {
        emit(HomeLoading());
        try {
          final banners = await bannerRepository.getAll();
          final latestProducts = await productRepository.getAll(ProductSort.latest);
          final popularProducts = await productRepository.getAll(ProductSort.popular);
          emit(HomeSuccess(banners: banners, latestProducts: latestProducts, popularProducts: popularProducts));
        } catch (e) {
          emit(HomeError(appExeption: e is AppExeption ? e : AppExeption()));
        }
      }
      // TODO: implement event handler
    });
  }
}
