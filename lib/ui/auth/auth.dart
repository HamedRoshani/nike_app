import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nike_app/data/repo/auth_repository.dart';
import 'package:nike_app/ui/auth/bloc/auth_bloc.dart';

class AuthScreen extends StatefulWidget {
  const AuthScreen({Key? key}) : super(key: key);

  @override
  State<AuthScreen> createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  TextEditingController usernameController = TextEditingController(text: "test@gmail.com");
  TextEditingController passwordController = TextEditingController(text: '123456');
  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    const Color onbackground = Colors.white;
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Theme(
        data: themeData.copyWith(
            elevatedButtonTheme: ElevatedButtonThemeData(
                style: ButtonStyle(
              shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(12))),
              minimumSize: MaterialStateProperty.all(const Size.fromHeight(56)),
              backgroundColor: MaterialStateProperty.all(onbackground),
              foregroundColor: MaterialStateProperty.all(themeData.colorScheme.secondary),
            )),
            snackBarTheme: SnackBarThemeData(backgroundColor: Theme.of(context).colorScheme.primary),
            colorScheme: themeData.colorScheme.copyWith(onSurface: Colors.white),
            inputDecorationTheme: const InputDecorationTheme(
                labelStyle: TextStyle(color: onbackground),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(12)),
                  borderSide: BorderSide(width: 2, color: Colors.white),
                ))),
        child: Scaffold(
          backgroundColor: themeData.colorScheme.secondary,
          body: BlocProvider<AuthBloc>(
            create: (context) {
              final bloc = AuthBloc(authrepository);
              bloc.add(AuthStarted());
              bloc.stream.forEach((state) {
                if (state is AuthSuccess) {
                  Navigator.of(context).pop();
                } else if (state is AuthError) {
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text(
                    state.exeption.message,
                  )));
                }
              });
              return bloc;
            },
            child: Padding(
              padding: const EdgeInsets.only(right: 48, left: 48),
              child: BlocBuilder<AuthBloc, AuthState>(
                buildWhen: (previous, current) {
                  return current is AuthError || current is AuthInitial || current is AuthLoading;
                },
                builder: (context, state) {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/img/nike_logo.png',
                        color: Colors.white,
                        width: 120,
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      Text(
                        state.islogin ? 'خوش آمدید' : 'ثبت نام',
                        style: TextStyle(color: onbackground, fontSize: 22),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      Text(
                        state.islogin ? 'لطفا وارد حساب کاربری خود شوید ' : 'لطفا ثبت نام کنید',
                        style: TextStyle(color: onbackground, fontSize: 16),
                      ),
                      const SizedBox(
                        height: 24,
                      ),
                      TextField(
                        controller: usernameController,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(label: Text('ادرس ایمیل')),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      _PasswordTextField(
                        controller: passwordController,
                      ),
                      const SizedBox(
                        height: 25,
                      ),
                      ElevatedButton(
                          onPressed: () {
                            BlocProvider.of<AuthBloc>(context).add(
                              AuthButtonIsClicked(usernameController.text, passwordController.text),
                            );
                          },
                          child: state is AuthLoading
                              ? CircularProgressIndicator()
                              : Text(
                                  state.islogin ? 'ورود' : 'ثبت نام',
                                )),
                      const SizedBox(
                        height: 24,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            state.islogin ? 'حساب کاربری ندارید ؟' : 'حساب کاربری دارید ؟',
                            style: TextStyle(color: onbackground.withOpacity(0.7)),
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          TextButton(
                              onPressed: () {
                                BlocProvider.of<AuthBloc>(context).add(AuthModeChangeIsClicked());
                              },
                              child: Text(state.islogin ? 'ثبت نام' : 'ورود',
                                  style: TextStyle(
                                    color: themeData.colorScheme.primary,
                                    decoration: TextDecoration.underline,
                                  )))
                        ],
                      )
                    ],
                  );
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _PasswordTextField extends StatefulWidget {
  const _PasswordTextField({
    Key? key,
    required this.controller,
  }) : super(key: key);

  final TextEditingController controller;

  @override
  State<_PasswordTextField> createState() => _PasswordTextFieldState();
}

class _PasswordTextFieldState extends State<_PasswordTextField> {
  late bool obsecureText = true;
  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: widget.controller,
      keyboardType: TextInputType.visiblePassword,
      obscureText: obsecureText,
      decoration: InputDecoration(
          suffixIcon: IconButton(
            onPressed: () {
              setState(() {
                obsecureText = !obsecureText;
              });
            },
            icon: Icon(
              obsecureText ? Icons.visibility_outlined : Icons.visibility_off_outlined,
            ),
            color: Colors.white54,
          ),
          label: const Text('رمز عبور')),
    );
  }
}
