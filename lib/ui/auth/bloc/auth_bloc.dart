import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:nike_app/common/exeption.dart';
import 'package:nike_app/data/repo/auth_repository.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final IAuthRepository authrepository;
  bool isloginMode;
  AuthBloc(this.authrepository, {this.isloginMode = true}) : super(AuthInitial(isloginMode)) {
    on<AuthEvent>((event, emit) async {
      try {
        if (event is AuthButtonIsClicked) {
          emit(AuthLoading(isloginMode));
          if (isloginMode) {
            await authrepository.logIn(event.username, event.password);
            emit(AuthSuccess(isloginMode));
          } else {
            await authrepository.signUp(event.username, event.password);
            emit(AuthSuccess(isloginMode));
          }
        } else if (event is AuthModeChangeIsClicked) {
          isloginMode = !isloginMode;
          emit(AuthInitial(isloginMode));
        }
      } catch (e) {
        emit(AuthError(isloginMode, AppExeption()));
      }
    });
  }
}
