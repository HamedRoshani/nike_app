part of 'auth_bloc.dart';

abstract class AuthState extends Equatable {
  const AuthState(this.islogin);
  final bool islogin;

  @override
  List<Object> get props => [islogin];
}

class AuthInitial extends AuthState {
  const AuthInitial(bool islogin) : super(islogin);
}

class AuthError extends AuthState {
  final AppExeption exeption;
  const AuthError(bool islogin, this.exeption) : super(islogin);
}

class AuthSuccess extends AuthState {
  const AuthSuccess(bool islogin) : super(islogin);
}

class AuthLoading extends AuthState {
  const AuthLoading(bool islogin) : super(islogin);
}
