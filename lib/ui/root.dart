import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nike_app/data/repo/auth_repository.dart';
import 'package:nike_app/ui/cart/cart.dart';
import 'package:nike_app/ui/home/home.dart';

class RootScreen extends StatefulWidget {
  const RootScreen({Key? key}) : super(key: key);

  @override
  State<RootScreen> createState() => _RootScreenState();
}

const int homeindex = 0;
const int cartIndex = 1;
const int profileIndex = 2;

class _RootScreenState extends State<RootScreen> {
  int selectedScreenindex = homeindex;

  final List<int> _history = [];

  GlobalKey<NavigatorState> homekey = GlobalKey();
  GlobalKey<NavigatorState> cartkey = GlobalKey();
  GlobalKey<NavigatorState> profilekey = GlobalKey();

  late final map = {
    homeindex: homekey,
    cartIndex: cartkey,
    profileIndex: profilekey,
  };

  Future<bool> onwillpop() async {
    final NavigatorState currentSelectedTapNavigationState = map[selectedScreenindex]!.currentState!;
    if (currentSelectedTapNavigationState.canPop()) {
      currentSelectedTapNavigationState.pop();
      return false;
    } else if (_history.isNotEmpty) {
      setState(() {
        selectedScreenindex = _history.last;
        _history.removeLast();
      });
      return false;
    }

    return true;
  }

  @override
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onwillpop,
      child: Scaffold(
        body: IndexedStack(
          index: selectedScreenindex,
          children: [
            _Navigator(homekey, homeindex, Homescreen()),
            _Navigator(cartkey, cartIndex, CartScreen()),
            _Navigator(
                profilekey,
                profileIndex,
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('profile'),
                    ElevatedButton(
                        onPressed: () {
                          authrepository.signout();
                        },
                        child: Text('sign out'))
                  ],
                )),
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: const [
            BottomNavigationBarItem(
              icon: Icon(
                CupertinoIcons.home,
              ),
              label: 'خانه',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                CupertinoIcons.cart,
              ),
              label: 'سبد خرید',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                CupertinoIcons.person,
              ),
              label: 'پروفایل',
            ),
          ],
          currentIndex: selectedScreenindex,
          onTap: (selectedIndex) {
            setState(() {
              selectedScreenindex = selectedIndex;
            });
          },
        ),
      ),
    );
  }

  Widget _Navigator(GlobalKey key, int index, Widget child) {
    return key.currentState == null && selectedScreenindex != index
        ? Container()
        : Navigator(
            key: key,
            onGenerateRoute: (settings) => MaterialPageRoute(
              builder: (context) => Offstage(offstage: selectedScreenindex != index, child: child),
            ),
          );
  }
}
