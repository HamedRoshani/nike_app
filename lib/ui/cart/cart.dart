import 'dart:ffi';
import 'dart:math';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:nike_app/common/utils.dart';
import 'package:nike_app/data/auth_info.dart';
import 'package:nike_app/data/repo/auth_repository.dart';
import 'package:nike_app/data/repo/cart_repository.dart';
import 'package:nike_app/ui/auth/auth.dart';
import 'package:nike_app/ui/cart/bloc/cart_bloc.dart';
import 'package:nike_app/ui/widgets/empty_state.dart';
import 'package:nike_app/ui/widgets/image.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({Key? key}) : super(key: key);

  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  late CartBloc? cartBloc;
  @override
  void initState() {
    super.initState();
    Authrepository.authChangeNotifier.addListener(authChangeNotifierListener);
  }

  void authChangeNotifierListener() {
    cartBloc?.add(CartAuthInfoChange(Authrepository.authChangeNotifier.value));
  }

  @override
  void dispose() {
    super.dispose();
    Authrepository.authChangeNotifier.removeListener(authChangeNotifierListener);
    cartBloc?.close();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xffF5F5F5),
        appBar: AppBar(
          centerTitle: true,
          title: const Text('سبد خرید'),
        ),
        body: BlocProvider<CartBloc>(
          create: (context) {
            final bloc = CartBloc(cartRepository);
            cartBloc = bloc;

            bloc.add(CartStart(Authrepository.authChangeNotifier.value));

            return bloc;
          },
          child: BlocBuilder<CartBloc, CartState>(
            builder: (context, state) {
              if (state is CartLoading) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              } else if (state is CartError) {
                return Center(
                  child: Text(state.appExeption.message),
                );
              } else if (state is CartSuccess) {
                return ListView.builder(
                  itemCount: state.cartResponse.carItems.length,
                  itemBuilder: (context, index) {
                    final data = state.cartResponse.carItems[index];
                    return Container(
                      margin: const EdgeInsets.all(4),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12),
                        boxShadow: [
                          BoxShadow(color: Colors.black.withOpacity(0.05), blurRadius: 10),
                        ],
                        color: Theme.of(context).colorScheme.surface,
                      ),
                      child: Column(children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              SizedBox(
                                width: 100,
                                height: 100,
                                child: ImageLoadingService(
                                  imageURL: data.product.imageurl,
                                  borderRadius: BorderRadius.circular(3),
                                ),
                              ),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    data.product.title,
                                    style: const TextStyle(fontSize: 16),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            left: 8,
                            right: 8,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                children: [
                                  const Text('تعداد'),
                                  Row(
                                    children: [
                                      IconButton(
                                        onPressed: () {},
                                        icon: const Icon(
                                          CupertinoIcons.plus_rectangle,
                                        ),
                                      ),
                                      Text(
                                        data.count.toString(),
                                        style: Theme.of(context).textTheme.headline6,
                                      ),
                                      IconButton(
                                        onPressed: () {},
                                        icon: const Icon(
                                          CupertinoIcons.minus_rectangle,
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    data.product.previousprice.withPriceLabel,
                                    style: const TextStyle(decoration: TextDecoration.lineThrough),
                                  ),
                                  Text(data.product.price.withPriceLabel),
                                ],
                              )
                            ],
                          ),
                        ),
                        const Divider(
                          height: 1,
                        ),
                        data.deletebuttonloading
                            ? const SizedBox(
                                height: 48,
                                child: Center(
                                  child: CupertinoActivityIndicator(),
                                ),
                              )
                            : TextButton(
                                onPressed: () {
                                  cartBloc?.add(CartDeleted(data.id));
                                },
                                child: const Text("حذف از سبد خرید"),
                              )
                      ]),
                    );
                  },
                );
              } else if (state is CartAuthRequired) {
                return EmptyView(
                    message: 'برای مشاهده سبد خرید ابتدا وارد حساب کاربری خود شوید',
                    calltoAction: ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => const AuthScreen(),
                          ));
                        },
                        child: Text('ورود')),
                    image: SvgPicture.asset(
                      'assets/img/auth_required.svg',
                      width: 140,
                    ));
              } else if (state is Cartempty) {
                return EmptyView(
                    message: 'تاکنون هیچ آیتمی به سبد خرید خود اضافه نکرده اید ',
                    image: SvgPicture.asset(
                      'assets/img/empty_cart.svg',
                      width: 200,
                    ));
              } else {
                throw Exception(' current state is not valid');
              }
            },
          ),
        )

        //    ValueListenableBuilder<AuthInfo?>(
        //     valueListenable: Authrepository.authChangeNotifier,
        //     builder: (context, authstate, child) {
        //       bool isAuthenticated = authstate != null && authstate!.accessToken.isNotEmpty;
        //       return Center(
        //         child: Column(
        //           mainAxisAlignment: MainAxisAlignment.center,
        //           children: [
        //             Text(isAuthenticated ? 'خوش آمدید' : "لطفا وارد حساب کاربری خود شوید "),
        //             isAuthenticated
        //                 ? ElevatedButton(
        //                     onPressed: () {
        //                       authrepository.signout();
        //                     },
        //                     child: const Text('خروج از حساب'))
        //                 : ElevatedButton(
        //                     onPressed: () {
        //                       Navigator.of(context, rootNavigator: true).push(MaterialPageRoute(
        //                         builder: (context) => AuthScreen(),
        //                       ));
        //                     },
        //                     child: const Text('لطفاوارد شوید '))
        //           ],
        //         ),
        //       );
        //     },
        //   ),
        );
  }
}
