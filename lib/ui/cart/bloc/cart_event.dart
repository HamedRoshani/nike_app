part of 'cart_bloc.dart';

abstract class CartEvent {
  const CartEvent();
}

class CartStart extends CartEvent {
  final AuthInfo? authInfo;

  const CartStart(this.authInfo);
}

class CartDeleted extends CartEvent {
  final int cartItemId;

  const CartDeleted(this.cartItemId);
}

class CartAuthInfoChange extends CartEvent {
  final AuthInfo? authInfo;

  const CartAuthInfoChange(this.authInfo);
}
