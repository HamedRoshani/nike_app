part of 'cart_bloc.dart';

abstract class CartState {
  const CartState();
}

class CartLoading extends CartState {}

class CartSuccess extends CartState {
  final CartResponse cartResponse;

  CartSuccess(this.cartResponse);
}

class CartError extends CartState {
  final AppExeption appExeption;

  CartError(this.appExeption);
}

class CartAuthRequired extends CartState {}

class Cartempty extends CartState {}
