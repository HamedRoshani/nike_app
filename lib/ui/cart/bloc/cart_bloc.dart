import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:nike_app/common/exeption.dart';
import 'package:nike_app/data/auth_info.dart';
import 'package:nike_app/data/cart_response.dart';
import 'package:nike_app/data/repo/cart_repository.dart';

part 'cart_event.dart';
part 'cart_state.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  final ICartRepository cartRepository;
  CartBloc(this.cartRepository) : super(CartLoading()) {
    on<CartEvent>(
      (event, emit) async {
        if (event is CartStart) {
          final authInfo = event.authInfo;
          if (authInfo == null || authInfo.accessToken.isEmpty) {
            emit(CartAuthRequired());
          } else {
            await loadCartItems(emit);
          }
        } else if (event is CartDeleted) {
          try {
            if (state is CartSuccess) {
              final successState = (state as CartSuccess);
              final cartItem = successState.cartResponse.carItems.firstWhere((element) => element.id == event.cartItemId);
              cartItem.deletebuttonloading = true;
              emit(CartSuccess(successState.cartResponse));
            }
            await cartRepository.delete(event.cartItemId);

            if (state is CartSuccess) {
              final successState = (state as CartSuccess);
              successState.cartResponse.carItems.removeWhere((element) => element.id == event.cartItemId);
              if (successState.cartResponse.carItems.isEmpty) {
                emit(Cartempty());
              } else {
                emit(CartSuccess(successState.cartResponse));
              }
            }
          } catch (e) {
            debugPrint(e.toString());
          }
        } else if (event is CartAuthInfoChange) {
          if (event.authInfo == null || event.authInfo!.accessToken.isEmpty) {
            emit(CartAuthRequired());
          } else {
            if (state is CartAuthRequired) {
              await loadCartItems(emit);
            }
          }
        }
      },
    );
  }
  Future<void> loadCartItems(Emitter<CartState> emit) async {
    try {
      emit(CartLoading());
      final resualt = await cartRepository.getAll();
      if (resualt.carItems.isEmpty) {
        emit(Cartempty());
      } else {
        emit(CartSuccess(resualt));
      }
    } catch (e) {
      emit(CartError(AppExeption()));
    }
  }
}
