import 'package:flutter/material.dart';
import 'package:nike_app/common/exeption.dart';

class AppErrorWidget extends StatelessWidget {
  final AppExeption appExeption;
  final GestureTapCallback ontap;
  const AppErrorWidget({
    Key? key,
    required this.appExeption,
    required this.ontap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(appExeption.message),
          ElevatedButton(
            onPressed: ontap,
            child: const Text('تلاش دوباره'),
          ),
        ],
      ),
    );
  }
}
