import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class EmptyView extends StatelessWidget {
  final String message;
  final Widget? calltoAction;
  final Widget image;
  const EmptyView({Key? key, required this.message, this.calltoAction, required this.image}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        image,
        Padding(
          padding: const EdgeInsets.only(left: 32, right: 32, top: 24, bottom: 16),
          child: Text(
            message,
            style: Theme.of(context).textTheme.headline6,
            textAlign: TextAlign.center,
          ),
        ),
        if (calltoAction != null) calltoAction!
      ],
    );
  }
}
