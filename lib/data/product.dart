class ProductSort {
  static const int latest = 0;
  static const int popular = 1;
  static const int priceHighToLow = 2;
  static const int priceLowToHigh = 3;
}

class ProductEntity {
  final int id;
  final String title;
  final String imageurl;
  final int price;
  final int discount;
  final int previousprice;

  ProductEntity.fromjson(Map<String, dynamic> json)
      : id = json['id'],
        title = json["title"],
        price = json['price'],
        imageurl = json['image'],
        discount = json['discount'],
        previousprice = json['previous_price'] ?? json['price'] + json['discount'];
}
