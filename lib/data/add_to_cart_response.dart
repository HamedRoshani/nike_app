class AddToCartResponse {
  final int productid;
  final int cartItemId;
  final int count;

  AddToCartResponse.fromjson(Map<String, dynamic> json)
      : productid = json['product_id'],
        cartItemId = json['id'],
        count = json['count'];
}
