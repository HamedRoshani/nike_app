import 'package:nike_app/common/http_Client.dart';
import 'package:nike_app/data/cart_item.dart';
import 'package:nike_app/data/add_to_cart_response.dart';
import 'package:nike_app/data/cart_response.dart';
import 'package:nike_app/data/sorce/cart_dataSorce.dart';

abstract class ICartRepository {
  Future<AddToCartResponse> add(int productid);
  Future<AddToCartResponse> changeCount(int cartItemId, int count);
  Future<void> delete(int cartItemId);
  Future<int> count();
  Future<CartResponse> getAll();
}

final cartRepository = CartRepository(CartRemoteDataSorce(httpClient));

class CartRepository extends ICartRepository {
  final ICartDataSorce dataSorce;

  CartRepository(this.dataSorce);
  @override
  Future<AddToCartResponse> add(int productid) => dataSorce.add(productid);

  @override
  Future<AddToCartResponse> changeCount(int cartItemId, int count) {
    // TODO: implement changeCount
    throw UnimplementedError();
  }

  @override
  Future<int> count() {
    // TODO: implement count
    throw UnimplementedError();
  }

  @override
  Future<void> delete(int cartItemId) {
    return dataSorce.delete(cartItemId);
  }

  @override
  Future<CartResponse> getAll() {
    return dataSorce.getAll();
  }
}
