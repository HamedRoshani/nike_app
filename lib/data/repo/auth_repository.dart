import 'package:flutter/cupertino.dart';
import 'package:nike_app/common/http_Client.dart';
import 'package:nike_app/data/auth_info.dart';
import 'package:nike_app/data/sorce/auth_data_sorce.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class IAuthRepository {
  Future<void> logIn(String username, String pasword);
  Future<void> signUp(String username, String pasword);
  Future<void> refreshToken();
  Future<void> signout();
}

final authrepository = Authrepository(AuthRemoteDataSource(httpClient));

class Authrepository implements IAuthRepository {
  static final ValueNotifier<AuthInfo?> authChangeNotifier = ValueNotifier(null);
  final IAuthDataSource authDatasource;

  Authrepository(this.authDatasource);
  @override
  Future<void> logIn(String username, String pasword) async {
    final AuthInfo authInfo = await authDatasource.logIn(username, pasword);
    _persistAuthTokens(authInfo);
    debugPrint('access Token Is' + authInfo.accessToken);
  }

  @override
  Future<void> refreshToken() async {
    if (authChangeNotifier.value != null) {
      final AuthInfo authInfo = await authDatasource.refreshToken(authChangeNotifier.value!.refreshToken);
      _persistAuthTokens(authInfo);
    }
  }

  @override
  Future<void> signUp(String username, String pasword) async {
    final AuthInfo authInfo = await authDatasource.sighnUp(username, pasword);
    _persistAuthTokens(authInfo);
    debugPrint('access Token Is' + authInfo.accessToken);
  }

  Future<void> _persistAuthTokens(AuthInfo authInfo) async {
    final SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString('access_token', authInfo.accessToken);
    sharedPreferences.setString('refresh_token', authInfo.refreshToken);
    loadAuthinfo();
  }

  Future<void> loadAuthinfo() async {
    final SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    final String accessToken = sharedPreferences.getString('access_token') ?? '';
    final String refreshToken = sharedPreferences.getString('refresh_token') ?? '';
    if (accessToken.isNotEmpty && refreshToken.isNotEmpty) {
      authChangeNotifier.value = AuthInfo(accessToken: accessToken, refreshToken: refreshToken);
    }
  }

  @override
  Future<void> signout() async {
    final SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.clear();
    authChangeNotifier.value = null;
  }
}
