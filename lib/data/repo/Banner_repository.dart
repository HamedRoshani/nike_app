import 'package:nike_app/common/http_Client.dart';
import 'package:nike_app/data/banner.dart';
import 'package:nike_app/data/sorce/Banner_data_sorce.dart';


final bannerRepository = BannerRepository(BannerDataSorce(httpClient));

abstract class IBannerRepository {
  Future<List<BannerEntity>> getAll();
}

class BannerRepository implements IBannerRepository {
  final IBannerdataorce iBannerdataorce;

  BannerRepository(this.iBannerdataorce);
  @override
  Future<List<BannerEntity>> getAll() => iBannerdataorce.getAll();
}
