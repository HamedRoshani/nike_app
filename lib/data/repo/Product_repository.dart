import 'package:nike_app/common/http_Client.dart';
import 'package:nike_app/data/product.dart';
import 'package:nike_app/data/sorce/Product_data_sorce.dart';


final productRepository = ProductRepository(ProductRemoteDataSorce(httpClient));

abstract class IProductRepository {
  Future<List<ProductEntity>> getAll(int sort);

  Future<List<ProductEntity>> search(String searchTerm);
}

class ProductRepository implements IProductRepository {
  final IProductDataSorce datasorce;

  ProductRepository(this.datasorce);
  @override
  Future<List<ProductEntity>> getAll(int sort) => datasorce.getAll(sort);

  @override
  Future<List<ProductEntity>> search(String searchTerm) =>
      datasorce.search(searchTerm);
}
