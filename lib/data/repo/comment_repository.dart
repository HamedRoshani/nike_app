import 'package:nike_app/common/http_Client.dart';
import 'package:nike_app/data/comment.dart';
import 'package:nike_app/data/sorce/comment_data_sorce.dart';

final commentRepository =
    CommentRepository(icommentDataSorce: CommentDataSorce(httpClient));

abstract class ICommentrepository {
  Future<List<CommentEntity>> getAll({required int productid});
}

class CommentRepository implements ICommentrepository {
  final IcommentDataSorce icommentDataSorce;

  CommentRepository({required this.icommentDataSorce});
  @override
  Future<List<CommentEntity>> getAll({required int productid}) =>
      icommentDataSorce.getAll(productid: productid);
}
