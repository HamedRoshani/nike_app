class BannerEntity {
  final int id;
  final String imageurl;

  BannerEntity.fromjason(Map<String, dynamic> fromjason)
      : id = fromjason['id'],
        imageurl = fromjason['image'];
}
