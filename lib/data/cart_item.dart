import 'dart:convert';

import 'package:nike_app/data/product.dart';

class CartItemEntity {
  final ProductEntity product;
  final int id;
  final int count;
  bool deletebuttonloading = false;

  CartItemEntity.fromjson(Map<String, dynamic> json)
      : product = ProductEntity.fromjson(json["product"]),
        id = json['cart_item_id'],
        count = json['count'];

  static List<CartItemEntity> parseJsonArry(List<dynamic> jsonArry) {
    final List<CartItemEntity> cartItems = [];
    jsonArry.forEach((element) {
      cartItems.add(CartItemEntity.fromjson(element));
    });
    return cartItems;
  }
}
