import 'package:dio/dio.dart';
import 'package:nike_app/common/exeption.dart';
import 'package:nike_app/data/common/response_Validatore.dart';
import 'package:nike_app/data/product.dart';

abstract class IProductDataSorce {
  Future<List<ProductEntity>> getAll(int sort);

  Future<List<ProductEntity>> search(String searchTerm);
}

class ProductRemoteDataSorce
    with ResponseValidatore
    implements IProductDataSorce {
  final Dio httpClient;

  ProductRemoteDataSorce(this.httpClient);
  @override
  Future<List<ProductEntity>> getAll(int sort) async {
    final response = await httpClient.get('product/list?sort=$sort');
    validateResponse(response);
    final products = <ProductEntity>[];
    (response.data as List).forEach((element) {
      products.add(ProductEntity.fromjson(element));
    });
    return products;
  }

  @override
  Future<List<ProductEntity>> search(String searchTerm) async {
    final response = await httpClient.get('product/search?q=$searchTerm');
    validateResponse(response);
    final products = <ProductEntity>[];
    (response.data as List).forEach((element) {
      products.add(ProductEntity.fromjson(element));
    });
    return products;
  }
}
