import 'package:dio/dio.dart';
import 'package:nike_app/common/exeption.dart';
import 'package:nike_app/data/banner.dart';
import 'package:nike_app/data/common/response_Validatore.dart';

abstract class IBannerdataorce {
  Future<List<BannerEntity>> getAll();
}

class BannerDataSorce with ResponseValidatore implements IBannerdataorce {
  final Dio httpclient;

  BannerDataSorce(this.httpclient);
  @override
  Future<List<BannerEntity>> getAll() async {
    final response = await httpclient.get('banner/slider');
    validateResponse(response);
    final banners = <BannerEntity>[];
    (response.data as List).forEach((element) {
      banners.add(BannerEntity.fromjason(element));
    });
    return banners;
  }
}
