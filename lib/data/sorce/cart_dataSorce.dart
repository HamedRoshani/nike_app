import 'package:dio/dio.dart';
import 'package:nike_app/data/cart_item.dart';
import 'package:nike_app/data/add_to_cart_response.dart';
import 'package:nike_app/data/cart_response.dart';

abstract class ICartDataSorce {
  Future<AddToCartResponse> add(int productid);
  Future<AddToCartResponse> changeCount(int cartItemId, int count);
  Future<void> delete(int cartItemId);
  Future<int> count();
  Future<CartResponse> getAll();
}

class CartRemoteDataSorce implements ICartDataSorce {
  final Dio httpClient;

  CartRemoteDataSorce(this.httpClient);

  @override
  Future<AddToCartResponse> add(int productid) async {
    final response = await httpClient.post('cart/add', data: {
      'product_id': productid,
    });
    return AddToCartResponse.fromjson(response.data);
  }

  @override
  Future<AddToCartResponse> changeCount(int cartItemId, int count) {
    // TODO: implement changeCount
    throw UnimplementedError();
  }

  @override
  Future<int> count() {
    // TODO: implement count
    throw UnimplementedError();
  }

  @override
  Future<void> delete(int cartItemId) async {
    await httpClient.post('cart/remove', data: {'cart_item_id': cartItemId});
  }

  @override
  Future<CartResponse> getAll() async {
    final response = await httpClient.get('cart/list');
    return CartResponse.fromjson(response.data);
  }
}
