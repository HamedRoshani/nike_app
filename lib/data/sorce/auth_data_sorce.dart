import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:nike_app/common/constants.dart';
import 'package:nike_app/data/auth_info.dart';
import 'package:nike_app/data/common/response_Validatore.dart';

abstract class IAuthDataSource {
  Future<AuthInfo> logIn(String username, String pasword);
  Future<AuthInfo> sighnUp(String username, String pasword);
  Future<AuthInfo> refreshToken(String token);
}

class AuthRemoteDataSource with ResponseValidatore implements IAuthDataSource {
  final Dio httpClient;

  AuthRemoteDataSource(this.httpClient);
  @override
  Future<AuthInfo> logIn(String username, String pasword) async {
    final response = await httpClient.post('auth/token', data: {
      "grant_type": "password",
      "client_id": 2,
      'client_secret': Constants.clientsecret,
      'username': username,
      "password": pasword,
    });
    validateResponse(response);

    return AuthInfo(accessToken: response.data['access_token'], refreshToken: response.data['refresh_token']);
  }

  @override
  Future<AuthInfo> refreshToken(String token) async {
    final response = await httpClient.post('auth/token', data: {
      'grant_type': 'refresh_token',
      'refresh_token': token,
      "client_id": 2,
      'client_secret': Constants.clientsecret,
    });
    validateResponse(response);

    return AuthInfo(accessToken: response.data["access_token"], refreshToken: response.data['refresh_token']);
  }

  @override
  Future<AuthInfo> sighnUp(String username, String pasword) async {
    final response = await httpClient.post('user/register', data: {
      "email": username,
      "password": pasword,
    });
    validateResponse(response);
    return logIn(username, pasword);
  }
}
