import 'package:dio/dio.dart';
import 'package:nike_app/data/comment.dart';
import 'package:nike_app/data/common/response_Validatore.dart';
import 'package:nike_app/data/common/response_Validatore.dart';

abstract class IcommentDataSorce {
  Future<List<CommentEntity>> getAll({required int productid});
}

class CommentDataSorce with ResponseValidatore implements IcommentDataSorce {
  final Dio httpClient;

  CommentDataSorce(this.httpClient);
  @override
  Future<List<CommentEntity>> getAll({required int productid}) async {
    final response = await httpClient.get('comment/list?product_id=$productid');
    validateResponse(response);
    final comments = <CommentEntity>[];
    (response.data as List).forEach((element) {
      comments.add(CommentEntity.fromjason(element));
    });
    return comments;
  }
}
