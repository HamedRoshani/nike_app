import 'package:dio/dio.dart';
import 'package:nike_app/common/exeption.dart';

mixin ResponseValidatore {
  validateResponse(Response response) {
    if (response.statusCode != 200) {
      throw AppExeption();
    }
  }
}
