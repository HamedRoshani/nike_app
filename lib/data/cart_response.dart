import 'package:nike_app/data/cart_item.dart';

class CartResponse {
  final List<CartItemEntity> carItems;
  final int pablePrice;
  final int totalPrice;
  final int shippingCost;

  CartResponse.fromjson(Map<String, dynamic> json)
      : carItems = CartItemEntity.parseJsonArry(json['cart_items']),
        pablePrice = json['payable_price'],
        totalPrice = json['total_price'],
        shippingCost = json["shipping_cost"];
}
