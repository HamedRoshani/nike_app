import 'package:flutter/material.dart';
import 'package:nike_app/data/product.dart';
import 'package:nike_app/data/repo/Banner_repository.dart';
import 'package:nike_app/data/repo/Product_repository.dart';
import 'package:nike_app/data/repo/auth_repository.dart';
import 'package:nike_app/theme.dart';
import 'package:nike_app/ui/auth/auth.dart';
import 'package:nike_app/ui/home/home.dart';
import 'package:nike_app/ui/root.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  authrepository.loadAuthinfo();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    const defualtTextStyle = TextStyle(fontFamily: "IranYekan", color: LightThemecolors.primarytextcolor);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
          appBarTheme: AppBarTheme(backgroundColor: Colors.white, foregroundColor: LightThemecolors.primarytextcolor, elevation: 0),
          snackBarTheme: SnackBarThemeData(contentTextStyle: defualtTextStyle.apply(color: Colors.white)),
          textTheme: TextTheme(
            subtitle1: defualtTextStyle.apply(color: LightThemecolors.secondarytextcolor),
            button: defualtTextStyle,
            bodyText2: defualtTextStyle,
            caption: defualtTextStyle.copyWith(color: LightThemecolors.secondarytextcolor),
            headline6: defualtTextStyle.copyWith(fontWeight: FontWeight.bold),
          ),
          colorScheme: const ColorScheme.light(
            primary: LightThemecolors.primarycolor,
            secondary: LightThemecolors.secondarycolor,
            onSecondary: Colors.white,
          )),
      home: const Directionality(textDirection: TextDirection.rtl, child: RootScreen()),
    );
  }
}
