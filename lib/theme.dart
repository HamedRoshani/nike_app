import 'package:flutter/cupertino.dart';

class LightThemecolors {
  static const primarycolor = Color(0xff217CF3);
  static const secondarycolor = Color(0xff262A35);
  static const primarytextcolor = Color(0xff262A35);
  static const secondarytextcolor = Color(0xffB3B6Be);
}
